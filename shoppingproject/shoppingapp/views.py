from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from .models import ShoppingItem, ShoppingList

from .forms import ItemForm, ListForm

# Create your views here.

def index(request):
    lists = ShoppingList.objects.all()
    return render(request, 'shoppingapp/index.html', {'lists':lists})

def detail(request, shoppinglist_id):
    liste = get_object_or_404(ShoppingList, pk=shoppinglist_id)
    items = ShoppingItem.objects.filter(shoppinglist=shoppinglist_id)
    summe = 0
    for entry in items:
        summe += entry.price * entry.quantity
    return render(request, 'shoppingapp/detail.html', {'items':items, 'liste':liste, 'summe':summe})

def new_list(request):
    name = request.POST['name']
    ShoppingList.objects.create(name=name)
    return HttpResponseRedirect(reverse('shoppingapp:index'))

def new_item(request):
    if request.method == 'POST':
        name = request.POST["name"]
        quantity = request.POST["quantity"]
        price = request.POST["price"]
        shoppinglist = ShoppingList.objects.get(pk=request.POST["shoppinglist"])
        ShoppingItem.objects.create(name=name,
                            quantity=quantity,
                            price=price,
                            shoppinglist=shoppinglist)
        return HttpResponseRedirect(reverse('shoppingapp:detail', args=[shoppinglist.id]))

    return render(request, 'shoppingapp/new_item_anlegen.html', {"shoppinglists": ShoppingList.objects.all()})

def updateItem(request, shoppingitem_id):
    item = get_object_or_404(ShoppingItem, pk=shoppingitem_id)

    form = ItemForm(instance=item)

    if request.method == 'POST':
        form = ItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('shoppingapp:index'))

    context = {'form':form}

    return render(request, 'shoppingapp/update_item.html', context)

def updateList(request, shoppinglist_id):
    liste = get_object_or_404(ShoppingList, pk=shoppinglist_id)

    form = ListForm(instance=liste)

    if request.method == 'POST':
        form = ListForm(request.POST, instance=liste)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('shoppingapp:index'))

    context = {'form':form}

    return render(request, 'shoppingapp/update_list.html', context)

def deleteItem(request, shoppingitem_id):
    item = get_object_or_404(ShoppingItem, pk=shoppingitem_id)
    item.delete()
    return HttpResponseRedirect(reverse('shoppingapp:index'))

def deleteList(request, shoppinglist_id):
    liste = get_object_or_404(ShoppingList, pk=shoppinglist_id)
    liste.delete()
    return HttpResponseRedirect(reverse('shoppingapp:index'))